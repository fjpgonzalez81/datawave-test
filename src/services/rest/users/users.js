import axios from 'axios';


function fetchUsers() {
  return axios.get('http://localhost:4000/users');
}


function fetchUser(userId) {
  return axios.get(`http://localhost:4000/users/${userId}`);
}

function fetchUserByEmail(email) {
  return axios.get(`http://localhost:4000/users?email=${email}`);
}

function updateUser(user) {
  user.lastUpdate = new Date();
  return axios.put(`http://localhost:4000/users/${user.id}`, user);
}

function removeUser(userId) {
  return axios.delete(`http://localhost:4000/users/${userId}`);
}

function addUser(user) {
  user.creationDate = new Date();
  user.lastUpdate = new Date();
  return axios.post('http://localhost:4000/users/', user);
}

export default {
  fetchUser,
  addUser,
  removeUser,
  updateUser,
  fetchUsers,
  fetchUserByEmail,
};
