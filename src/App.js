import React, { Fragment } from 'react';
import './App.css';
import { Footer } from './components';


const App = ({ children }) => <Fragment>
  {children}
  <Footer />
</Fragment>;

export default App;
