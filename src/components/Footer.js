import React from 'react';
import { PropTypes } from 'prop-types';

const Footer = () => (
  <nav id="footer" className="navbar is-fixed-bottom ">
    <div className="column is-fullwidth">
      <div className="content has-text-centered has-text-weight-bold">
        <p>
          <strong><i className="fa fa-copyright" aria-hidden="true" /> Francisco Parra Gonzalez</strong>
        </p>
      </div>
    </div>
  </nav>
);


Footer.propTypes = {
  classes: PropTypes.object,
};

export default Footer;
