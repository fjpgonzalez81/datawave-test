import React, { Component } from 'react';
import UserList from './UserList';
import { Button } from 'primereact/components/button/Button';
// import UserProfile from './UserProfile';


export default class UserManagement extends Component {


  render() {
    return (
      <div className="container has-text-centered section">
        <p className="title is-bold is-uppercase is-4">User Management System</p>
        <nav className="level" style={{ marginBottom: 0, display: 'block' }}>
          <div className="level-right">
            <Button
              type="button" icon="fa-plus"
              className="ui-button-success level-item" onClick={() => this.props.history.push('/register')}
            >
        Add user
    </Button>
          </div>
        </nav>
        <UserList {...this.props} />
      </div>
    );
  }

}

