import React, { Component } from 'react';
import { Button } from 'primereact/components/button/Button';
import { Link } from 'react-router-dom';

export default class UserProfile extends Component {

  constructor(props) {
    super(props);
    this.state = { isEditionMode: false, user: {} };
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.actions.fetchUser(id);
  }

  componentWillReceiveProps(props) {
    this.setState({ user: props.user });
  }


  onCancel() {
    const { id } = this.props.match.params;
    this.props.actions.fetchUser(id);
    this.setState({ isEditionMode: false });
  }

  updateUser() {
    this.props.actions.updateUser(this.state.user);
    this.setState({ isEditionMode: false });
  }

  handleChange(e) {
    const user = this.state.user;
    user[e.target.id] = e.target.value;
    this.setState({ user });
  }

  userNotFound() {
    return (
      <div className="container section has-text-centered">
        <p className="title is-bold has-text-danger">User not found</p>
        <Link to="/users">
          <span>Back to users</span>
        </Link>
      </div>
    );
  }

  renderForm() {
    return (
      <form>
        <div className="field">
          <label htmlFor="name" className="label">Name {this.state.isEditionMode && <span style={{ color: 'red' }}>*</span>}</label>
          <div className="control">
            {!this.state.isEditionMode && this.state.user.name}
            {this.state.isEditionMode &&
            <input
              id="name" name="name" className="input" type="text" placeholder="Name"
              value={this.state.user.name} onChange={e => this.handleChange(e)}
            />}
          </div>
        </div>


        <div className="field">
          <label htmlFor="email" className="label">Email {this.state.isEditionMode && <span style={{ color: 'red' }}>*</span>}</label>
          <div className="control">
            {this.state.user.email}
          </div>
        </div>

        <div className="field">
          <label htmlFor="role" className="label">Role {this.state.isEditionMode && <span style={{ color: 'red' }}>*</span>}</label>
          <div className="control">
            {!this.state.isEditionMode && this.state.user.role}
            {this.state.isEditionMode &&
            <input
              id="role" name="role" className="input" type="text" placeholder="Role"
              value={this.state.user.role} onChange={e => this.handleChange(e)}
            />}
          </div>
        </div>

        <div className="field">
          <label htmlFor="lastUpdate" className="label">Last update</label>
          <div className="control">
            {this.state.user.lastUpdate && this.state.user.lastUpdate.toString()}
          </div>
        </div>

        <div className="field">
          <label htmlFor="creationDate" className="label">Creation date</label>
          <div className="control">
            {this.state.user.creationDate && this.state.user.creationDate.toString()}
          </div>
        </div>

        <div className="field">
          { !this.state.isEditionMode && this.state.user.description && <label htmlFor="description" className="label">Description</label>}
          <div className="control">
            {!this.state.isEditionMode && this.state.user.description}
            {this.state.isEditionMode && <textarea
              id="description" name="description" value={this.state.user.description}
              className="textarea is-primary" type="text" placeholder="Description" onChange={e => this.handleChange(e)}
            />}
          </div>
        </div>
      </form>

    );
  }

  renderActionButton() {
    const actionButton = this.state.isEditionMode
            ? (<Button
              disabled={!this.state.user.name || !this.state.user.email || !this.state.user.role}
              type="button" icon="fa-save"
              className="ui-button-success" onClick={() => this.updateUser()}
            >
                Update user
            </Button>)
            :
            (<Button
              type="button" icon="fa-edit"
              className="ui-button-info" onClick={() => this.setState({ isEditionMode: true })}
            >
                Edit user
            </Button>);


    return actionButton;
  }


  renderProfile() {
    const { user } = this.props;
    return user && (
    <div className="container section">

      <p className="title is-bold is-4">Welcome to the profile of {user.name}</p>


      <div className="columns">

        <div className="column is-half">
          {this.renderForm()}

          <div style={{ marginTop: '3vh' }}>
            {this.renderActionButton()}

            {this.state.isEditionMode && <Button
              type="button" icon="fa-ban"
              className="ui-button-warning"
              onClick={() => this.onCancel()}
            >
                                Cancel Edition
                            </Button>}
            <div className="is-pulled-right">
              <Link to="/users">
                <span>Back to users</span>
              </Link>
            </div>
          </div>

        </div>

      </div>
    </div>
        );
  }


  render() {
    return this.renderProfile() || this.userNotFound();
  }
}
