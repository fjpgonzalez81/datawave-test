import React, { Component } from 'react';
import { Button } from 'primereact/components/button/Button';
import { Link } from 'react-router-dom';


export default class UserRegister extends Component {


  constructor(props) {
    super(props);
    this.state = { user: {} };
  }


  componentWillReceiveProps(props) {
    const { redirect } = props;
    if (redirect) {
      alert('User created succesfully');
      this.props.history.push('/users');
      this.props.actions.setRedirect(false);
    }
  }

  handleChange(e) {
    const user = this.state.user;
    user[e.target.id] = e.target.value;
    this.setState({ user });
  }

  saveUser() {
    this.props.actions.addUser(this.state.user);
  }

  render() {
    return (
      <div className="container section">

        <p className="title is-bold is-4">Create user</p>


        <div className="columns">

          <div className="column is-half">
            <form>
              <div className="field">
                <label htmlFor="name" className="label">Name <span style={{ color: 'red' }}>*</span></label>
                <div className="control">
                  <input
                    id="name" name="name" className="input" type="text" placeholder="Name"
                    value={this.state.user.name} onChange={e => this.handleChange(e)}
                  />
                </div>
              </div>

              <div className="field">
                <label htmlFor="password" className="label">Password <span style={{ color: 'red' }}>*</span></label>
                <div className="control">
                  <input
                    id="password" name="password" className="input" type="password" placeholder="Password"
                    value={this.state.user.password} onChange={e => this.handleChange(e)}
                  />
                </div>
              </div>


              <div className="field">
                <label htmlFor="email" className="label">Email <span style={{ color: 'red' }}>*</span></label>
                <div className="control">
                  <input
                    id="email" cname="email" className="input" type="text" placeholder="Email"
                    value={this.state.user.email} onChange={e => this.handleChange(e)}
                  />
                </div>
              </div>

              <div className="field">
                <label htmlFor="role" className="label">Role <span style={{ color: 'red' }}>*</span></label>
                <div className="control">
                  <input
                    id="role" name="role" className="input" type="text" placeholder="Role"
                    value={this.state.user.role} onChange={e => this.handleChange(e)}
                  />
                </div>
              </div>

              <div className="field">
                <label htmlFor="description" className="label">Description</label>
                <div className="control">
                  <textarea
                    id="description" name="description" value={this.state.user.description}
                    className="textarea" type="text" placeholder="Description"
                    onChange={e => this.handleChange(e)}
                  />
                </div>
              </div>

              <Button
                disabled={!this.state.user.name || !this.state.user.password
                || !this.state.user.email || !this.state.user.role}
                type="button" icon="fa-save"
                className="ui-button-success" onClick={() => this.saveUser()}
              >
                        Save user
                    </Button>
              <div className="is-pulled-right">
                <Link to="/users">
                  <span>Back to users</span>
                </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
