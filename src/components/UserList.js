import React, { Component, Fragment } from 'react';
import Loading from './Loading';
import { DataTable } from 'primereact/components/datatable/DataTable';
import { Column } from 'primereact/components/column/Column';
import { Button } from 'primereact/components/button/Button';

export default class UserList extends Component {

  constructor(props) {
    super(props);
    this.state = { userIdToRemove: null, classNames: '' };
  }

  componentDidMount() {
    this.props.actions.fetchUsers();
  }

  actionTemplate(user, column) {
    console.log(column);
    return (<div>
      <Button
        type="button" icon="fa-search"
        className="ui-button-success" onClick={() => this.selectUser(user.id)}
      />
      <Button
        type="button" icon="fa-remove"
        className="ui-button-danger" onClick={() => this.confirmUserRemoval(user.id)}
      />
    </div>);
  }

  selectUser(userId) {
    this.props.history.push(`/users/${userId}`);
  }


  removeUser() {
    this.props.actions.removeUser(this.state.userIdToRemove);
    this.setState({ userIdToRemove: null, classNames: '' });
  }

  confirmUserRemoval(userId) {
    this.setState({ userIdToRemove: userId, classNames: 'is-active' });
  }

  cancelUserRemoval() {
    this.setState({ userIdToRemove: null, classNames: '' });
  }


  renderUsers() {
    const { users } = this.props;
    return users && (<ol>
      {
            users.map(user => <li key={user.email}>{user.email}</li>)}
    </ol>);
  }


  renderTable() {
    const { users } = this.props;
    return (
      <DataTable
        value={users} responsive paginator
        rows={5} rowsPerPageOptions={[5, 10, 20]}
      >
        <Column field="name" header="Name" filter style={{ textAlign: 'center', width: '6em' }} />
        <Column field="email" header="Email" filter style={{ textAlign: 'center', width: '6em' }} />
        <Column body={this.actionTemplate.bind(this)} header="Actions" style={{ textAlign: 'center', width: '6em' }} />
      </DataTable>
    );
  }

  renderDeleteConfirmationModal() {
    const classNames = `modal ${this.state.classNames}`;
    return (
      <div className={classNames}>
        <div className="modal-background" />
        <div className="modal-content" >
          <div className="box">
            <p>
                            Please confirm that you want to delete this user
                        </p>
            <Button
              type="button" icon="fa-remove"
              className="ui-button-danger" onClick={() => this.removeUser()}
            >
                            Delete
                        </Button>
          </div>
        </div>
        <button className="modal-close is-large" aria-label="close" onClick={() => this.cancelUserRemoval()} />
      </div>
    );
  }


  render() {
    return (
      <Fragment>{this.renderTable() || <Loading />}
        {this.renderDeleteConfirmationModal()}
      </Fragment>);
  }


}
