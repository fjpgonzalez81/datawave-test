import {
    FETCH_USERS_REQUESTED,
    FETCH_USERS_SUCCEEDED,
    FETCH_USER_REQUESTED,
    FETCH_USER_SUCCEEDED,
    REMOVE_USER_REQUESTED,
    REMOVE_USER_SUCCEEDED,
    ADD_USER_REQUESTED,
    ADD_USER_SUCCEEDED,
    UPDATE_USER_REQUESTED,
    UPDATE_USER_SUCCEEDED,
    ADD_USER_FAILED,
    SET_REDIRECT,
} from '../constants';


const fetchUsers = payload => ({ type: FETCH_USERS_REQUESTED, payload });
const fetchUser = payload => ({ type: FETCH_USER_REQUESTED, payload });
const removeUser = payload => ({ type: REMOVE_USER_REQUESTED, payload });
const addUser = payload => ({ type: ADD_USER_REQUESTED, payload });
const updateUser = payload => ({ type: UPDATE_USER_REQUESTED, payload });
const setRedirect = payload => ({ type: SET_REDIRECT, payload });

export const userListActions = {
  fetchUsers,
  removeUser,
};

export const userProfileActions = {
  fetchUser,
  updateUser,
};

export const userRegisterActions = {
  addUser,
  setRedirect,
};

const initialState = {
  users: null,
  selectedUser: null,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {

    case FETCH_USERS_SUCCEEDED: {
      const { data } = action.users;
      return Object.assign({}, state, { users: data });
    }

    case FETCH_USER_SUCCEEDED: {
      const { data } = action.user;
      return Object.assign({}, state, { selectedUser: data });
    }

    case REMOVE_USER_SUCCEEDED: {
      const { userId } = action;
      const users = state.users.filter(user => user.id !== userId);
      return Object.assign({}, state, { users });
    }

    case ADD_USER_SUCCEEDED: {
      const { user } = action;
      const users = state.users ? [...state.users] : [];
      users.push(user);
      return Object.assign({}, state, { redirect: true, users });
    }

    case ADD_USER_FAILED: {
      alert(action.message);
      return state;
    }

    case UPDATE_USER_SUCCEEDED: {
      const { user } = action;
      alert('User updated succesfully');
      return Object.assign({}, state, { selectedUser: user });
    }
    case SET_REDIRECT: {
      return Object.assign({}, state, { redirect: false });
    }
    default:
      return state;
  }
};

export default appReducer;

