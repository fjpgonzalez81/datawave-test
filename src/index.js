// import React from 'react';
// import ReactDOM from 'react-dom';
import './index.css';
import 'bulma/css/bulma.css';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/omega/theme.css';
import 'font-awesome/css/font-awesome.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';
import routes from './routes';
import { render } from 'react-dom';

// ReactDOM.render(<App />, document.getElementById('root'));

render(routes, document.getElementById('root'));

registerServiceWorker();
