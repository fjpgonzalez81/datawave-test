import { call, put, takeEvery } from 'redux-saga/effects';
import { usersAPI } from '../services/rest';
import {
    FETCH_USERS_REQUESTED,
    FETCH_USERS_SUCCEEDED,
    FETCH_USERS_FAILED,
    FETCH_USER_REQUESTED,
    FETCH_USER_SUCCEEDED,
    FETCH_USER_FAILED,
    REMOVE_USER_REQUESTED,
    REMOVE_USER_SUCCEEDED,
    REMOVE_USER_FAILED,
    ADD_USER_REQUESTED,
    UPDATE_USER_REQUESTED,
    ADD_USER_SUCCEEDED,
    ADD_USER_FAILED,
    UPDATE_USER_SUCCEEDED,
    UPDATE_USER_FAILED,
} from '../constants';

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser(action) {
  try {
    const user = yield call(usersAPI.fetchUser, action.payload);
    yield put({ type: FETCH_USER_SUCCEEDED, user });
  } catch (e) {
    yield put({ type: FETCH_USER_FAILED, message: e.message });
  }
}

function* removeUser(action) {
  try {
    yield call(usersAPI.removeUser, action.payload);
    yield put({ type: REMOVE_USER_SUCCEEDED, userId: action.payload });
  } catch (e) {
    yield put({ type: REMOVE_USER_FAILED, message: e.message });
  }
}

function* fetchUsers() {
  try {
    const users = yield call(usersAPI.fetchUsers);
    yield put({ type: FETCH_USERS_SUCCEEDED, users });
  } catch (e) {
    yield put({ type: FETCH_USERS_FAILED, message: e.message });
  }
}

function* addUser(action) {
  try {
    const { data } = yield call(usersAPI.fetchUserByEmail, action.payload.email);
    if (data.length > 0) {
      yield put({ type: ADD_USER_FAILED, message: 'There is already a user with that email' });
    } else {
      const result = yield call(usersAPI.addUser, action.payload);
      const user = result.data;
      yield put({ type: ADD_USER_SUCCEEDED, user });
    }
  } catch (e) {
    yield put({ type: ADD_USER_FAILED, message: e.message });
  }
}

function* updateUser(action) {
  try {
    yield call(usersAPI.updateUser, action.payload);
    yield put({ type: UPDATE_USER_SUCCEEDED, user: action.payload });
  } catch (e) {
    yield put({ type: UPDATE_USER_FAILED, message: e.message });
  }
}

/*
  Starts fetchUser on each dispatched `USER_FETCH_REQUESTED` action.
  Allows concurrent fetches of user.
*/
function* fetchUserSaga() {
  yield takeEvery(FETCH_USER_REQUESTED, fetchUser);
}

function* fetchUsersSaga() {
  yield takeEvery(FETCH_USERS_REQUESTED, fetchUsers);
}

function* removeUserSaga() {
  yield takeEvery(REMOVE_USER_REQUESTED, removeUser);
}

function* addUserSaga() {
  yield takeEvery(ADD_USER_REQUESTED, addUser);
}

function* updateUserSaga() {
  yield takeEvery(UPDATE_USER_REQUESTED, updateUser);
}

/*
  Alternatively you may use takeLatest.

  Does not allow concurrent fetches of user. If "USER_FETCH_REQUESTED" gets
  dispatched while a fetch is already pending, that pending fetch is cancelled
  and only the latest one will be run.
*/
// function* mySaga() {
//   yield takeLatest('USER_FETCH_REQUESTED', fetchUser);
// }

export default [fetchUsersSaga, fetchUserSaga, removeUserSaga, addUserSaga, updateUserSaga];
