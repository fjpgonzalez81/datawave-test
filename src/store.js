import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import sagas from './sagas/sagas';

import appReducer from './modules/app-module';

export default function createAppStore(browserHistory) {
  const rootReducer = combineReducers({
    app: appReducer,
    routing: routerReducer,
  });

// const rootReducer = () => ({
//   setUser,
// });

    // create the saga middleware
  const sagaMiddleware = createSagaMiddleware();
// mount it on the Store

  const middlewares = [
    routerMiddleware(browserHistory),
    sagaMiddleware,
  ];

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(rootReducer,
        composeEnhancers(
            applyMiddleware(...middlewares),
        ),
    );

  sagas.forEach((saga) => {
    sagaMiddleware.run(saga);
  });

  return store;
}

