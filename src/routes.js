import React from 'react';
import { Provider } from 'react-redux';
import { Route, Switch, Link } from 'react-router-dom';
import { UserContainer, UserProfileContainer, UserRegisterContainer } from './containers';
import App from './App';
import createAppStore from './store';
import { ConnectedRouter } from 'react-router-redux';


import { history } from './history';

const store = createAppStore(history);

const Home = () => <div className="container has-text-centered section">
  <p className="title is-bold is-3">Hi! and welcome to the user management test, click on List users button to begin</p>
  <Link to="/users">
    <a href className="button is-primary">List users</a>
  </Link>
</div>;

const NoMatch = () => <div>No match</div>;

const routes = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/users" component={UserContainer} />
          <Route exact path="/users/:id" component={UserProfileContainer} />
          <Route exact path="/register" component={UserRegisterContainer} />
          <Route component={NoMatch} />
        </Switch>
      </App>
    </ConnectedRouter>

  </Provider>);


export default routes;
