import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserProfile } from '../components';
import { userProfileActions } from '../modules/app-module';


// mapStateToProps :: {State} -> {Props}
const mapStateToProps = state => ({
  user: state.app.selectedUser,
});


// mapDispatchToProps :: Dispatch -> {Action}
const mapDispatchToProps = dispatch => ({
  actions: {
    ...bindActionCreators(
        userProfileActions,
            dispatch,
        ),
  },
});

const Component = UserProfile;

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);
