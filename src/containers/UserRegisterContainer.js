import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserRegister } from '../components';
import { userRegisterActions } from '../modules/app-module';


// mapStateToProps :: {State} -> {Props}
const mapStateToProps = state => ({
  redirect: state.app.redirect,
})
;


// mapDispatchToProps :: Dispatch -> {Action}
const mapDispatchToProps = dispatch => ({
  actions: {
    ...bindActionCreators(
            userRegisterActions,
            dispatch,
        ),
  },
});

const Component = UserRegister;

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);
