import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserManagement } from '../components';
import { userListActions } from '../modules/app-module';


// mapStateToProps :: {State} -> {Props}
const mapStateToProps = state => ({
  users: state.app.users,
  selectedUser: state.app.selectedUser,
});


// mapDispatchToProps :: Dispatch -> {Action}
const mapDispatchToProps = dispatch => ({
  actions: {
    ...bindActionCreators(
        userListActions,
            dispatch,
        ),
  },
});

const Component = UserManagement;

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);
