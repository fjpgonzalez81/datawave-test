Instructions to access the application:

You need to haved installed in your local machine:

git - https://git-scm.com/
node - https://nodejs.org/en/
npm - (it comes with node installation)

Just in case, in my local environment I have used the version (on a mac):

git: 2.11.1
node: v9.5.0
npm: 5.6.0


With all of the above installed, open a terminal and clone the repository and execute the following commands:

git clone https://YOUR_BITBUCKET_USERNAME@bitbucket.org/fjpgonzalez81/datawave-test.git datawave-test  (clone the repo locally in the folder datawave-test)
cd datawave-test (navigate to that folder)
npm install (install all required dependencies, if you are under a corporate proxy, check the configuration of your proxy)
npm run start

This should start the application in the port 3000, with an API json server in the port 4000, so all you need is to access
http://localhost:3000/ in your local browser to start the application.

Technologies used for the application:

As specified in the specs, I have used React, Redux and React Saga, for the css I have used Bulma and a component of PrimeReact (Datatable).
The major operations of a user management system have been implemented.
So we can list the users of the system, modify, delete them, and also create new users. I have assumed that every user has a unique email address,
so it is not possible create two users with the same email address.
The system comes with 3 predefined users, and the table has been set up to use client pagination. The number of max user displayed per page has been
set to 5 (but this can be changed) so if more users are created, if will be necessary to navigate between pages to see the users.

The main idea was to keep the first screen (the list of users) with only the essential data (name and email),
to not overload the screen and load fast, and then if we want to know all the details of the user, we load the specific details only for
the selected user.
The design has taken in account responsiveness, to be able to see correctly the application in different screen sizes and port widths.




